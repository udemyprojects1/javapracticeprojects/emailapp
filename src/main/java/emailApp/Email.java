package emailApp;

import java.util.Scanner;

public class Email {

    private String firstName;
    private String lastName;
    private String password;
    private String department;
    private int mailBoxCapacity = 500;
    private String alternateMail;
    private int passwordLengthDefault = 8;
    private String email;
    private String companySuffix = "aeycompany.com";


    //Constructor for firstname and lastname
    public Email(String firstName, String lastName) {

        this.firstName = firstName;
        this.lastName = lastName;

        //Call a method asking for the department
        this.department = setDepartment();

        //Call a method that returns random password
        this.password = randomPassword(passwordLengthDefault);
        System.out.println("Your password: " + this.password);

        //Combine elements to generate email.
        this.email = firstName.toLowerCase()
                + "."
                + lastName.toLowerCase()
                + "@"
                + department
                + "."
                + companySuffix;

        //If department is none, delete dot
        if (this.department.equals("")) {
            int indexOfEta = this.email.indexOf('@') + 1;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(this.email, 0, indexOfEta);
            stringBuilder.append(this.email.substring(indexOfEta + 1));
            this.email = stringBuilder.toString();
        }


    }

    //Ask for the department

    private String setDepartment() {
        System.out.print("New worker: " + firstName + "\n" +
                "Possible departments:\n" +
                "1 for Sales\n" +
                "2 for Development\n" +
                "3 for Accounting\n" +
                "4 for None\n" +
                "Enter the department:");
        Scanner in = new Scanner(System.in);
        int departmentChoice = in.nextInt();
        in.nextLine();
        String dep;
        if (departmentChoice == 1) {
            dep = "sales";
        } else if (departmentChoice == 2) {
            dep = "dev";
        } else if (departmentChoice == 3) {
            dep = "acct";
        } else {
            dep = "";
        }

        return dep;

    }
    //Generate random password

    private String randomPassword(int length) {
        String passwordSet = "ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789!@#$%^%";
        char[] password = new char[length];
        for (int i = 0; i < length; i++) {
            int rand = (int) (Math.random() * passwordSet.length());
            password[i] = passwordSet.charAt(rand);
        }
        return new String(password);
    }

    //Set the mailbox capacity
    public void setMailBoxCapacity(int capacity) {
        this.mailBoxCapacity = capacity;

    }

    //Set alternate mail
    public void setAlternateMail(String alternateMail) {
        this.alternateMail = alternateMail;
    }

    //Change the password
    public void setPassword(String password) {
        this.password = password;
    }

    //Get mailbox capacity
    public int getMailBoxCapacity() {
        return this.mailBoxCapacity;
    }

    //Get alternate email
    public String getAlternateMail() {
        return this.alternateMail;
    }

    //Get password
    public String getPassword() {
        return this.password;
    }

    //Show info
    public String showInfo() {
        return "\nDisplay name: " + firstName + " " + lastName +
                "\nCompany email: " + email +
                "\nMailbox capacity: " + mailBoxCapacity + " Mb.";
    }

}
